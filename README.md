**Read Me First**

This repository will hold a few examples that are related to the Anaplan Best Practices series titled [Anaplan and Python](https://community.anaplan.com/t5/Best-Practices/tkb-p/communitykb). If you want to contribute to this repo, please message me on the Anaplan Community, or make a pull request to this repo. 

---

## Getting Started

1. The guide, code examples and all material in this repository has been worked through using Python 3.9.2, and an installation of Visual Studio Code on Windows 10 as the editor. All code should be compatible with other installations of Python and other operating systems, noting any version dependencies/conflicts. 
2. Pip is used as the package installation manager.
3. I'm happy to answer questions directed on the Anaplan Community forum, but please ensure you have a go first! I can only help you, if you've tried to help yourself. 

---

## Folder structure

The repository folder structure is as follows: 

    .
    ├── articles                    # Top level folder per article
    │   ├── article_1               # Content related to article 1
    │   ├── article_2               # Content related to article 2
    │   ├── ...
    ├── outputs                     # Top level folder for example output (optional)
    │   ├── article_1               # Example output for article 1
    │   ├── article_2               # Example output for article 2
    │   ├── ...
    ├── tools                       # Top level folder for tools
    │   ├── tool_1                  
    │   ├── ...
    ├── requirements.txt            # Requirements file for pip
    ├── LICENSE
    └── README.md

---

## Clone this repository

Steps to come to ensure you can just clone this repo and use it