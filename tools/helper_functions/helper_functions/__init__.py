# pylint: skip-file
from base64 import b64encode
import os
import requests
from OpenSSL import crypto
import random
import string
import json
import time

class Error(Exception):
    pass

def replace_escaped_returns(input_data, escape_char = '"', to_replace = ['\r', '\n'], replace_with = ' '):
    list_char = list(input_data)
    index = 0
    check = 0
    while index < len(list_char):
        check = index
        if list_char[index] == escape_char:
            check = index + 1
            while check < len(list_char):
                if list_char[check] in to_replace:
                    list_char[check] = replace_with
                if list_char[check] == escape_char:
                    break
                check += 1
                continue
        index = check + 1
    result = ''.join(list_char)
    return result

def get_ca_parameters(local_path, cert_path, cert_file, key_file, passphrase=None, saved_token_file='BLANK', save_token_name='saved.token'):
    if saved_token_file != 'BLANK':
        with open(local_path + '/' + saved_token_file) as saved_token:
            saved_token_data = json.load(saved_token)
            if saved_token_data['expiresAt'] > time.time():
                print('returning saved token data')
                return saved_token_data['tokenValue']
        return 

    cert_path = local_path + "/cert"
    certfile = cert_path+"/anaplan_system_integration_user.pem"
    keyfile = cert_path+"/Anaplan.key"
    st_cert=open(certfile, 'rt').read()
    st_key=open(keyfile, 'rt').read()
    key=crypto.load_privatekey(crypto.FILETYPE_PEM, st_key,passphrase=passphrase)

    random_str = os.urandom(100)
    signed_str = crypto.sign(key, random_str, "sha512")

    auth_value = 'CACertificate %s' % (st_cert.replace("\n", "").replace("-----BEGIN CERTIFICATE-----", "").replace("-----END CERTIFICATE-----", ""))
    encodedstr = b64encode(random_str)
    signedstr = b64encode(signed_str)
    body = '{\"encodedData\": \"' + encodedstr.decode("utf-8") + '\", \"encodedSignedData\": \"' + signedstr.decode("utf-8") + '\"}'

    auth_headers = {}
    auth_headers['Authorization'] = auth_value
    auth_url = "https://auth.anaplan.com/token/authenticate"

    auth_response = requests.post(auth_url, headers=auth_headers, data=body)
    auth_token = auth_response.json().get('tokenInfo')
    with open(save_token_name, 'w') as out_file:
        json.dump(auth_token, out_file)

    return auth_token['tokenValue']


def refresh_auth_token(auth_token):
    auth_headers = {}
    auth_headers['Authorization'] = auth_token
    refresh_url = "https://auth.anaplan.com/token/validate"
    requests.post(refresh_url, headers=auth_headers)
    

def run_action(workspace_id, model_id, action_type, action_id, auth_token='', file_id='', body=''):
    valid_actions = ['imports', 'exports', 'processes','upload']
    try:
        if action_type not in valid_actions:
            raise Error
        base_url = "https://api.anaplan.com/2/0/workspaces/" + workspace_id + "/models/" + model_id
        print(action_type)
        #action_url = base_url + "/" + action_type + "/" + action_id
    except:
       print('The action type was invalid')
       return

    if action_type == 'upload':
        if file_id:
            upload_headers = {}
            upload_headers['Authorization'] = 'AnaplanAuthToken ' + auth_token
            upload_headers['Content-Type'] = 'application/octet-stream'
            upload_url = 'https://api.anaplan.com/2/0/workspaces/{}/models/{}/files/{}/'.format(workspace_id, model_id, file_id)
            return(requests.put(upload_url, headers=upload_headers, data=body))
    elif action_type == 'imports':
        import_headers = {}
        import_headers['Authorization'] = 'AnaplanAuthToken ' + auth_token
        import_headers['Content-Type'] = 'application/json'
        import_url = 'https://api.anaplan.com/2/0/workspaces/{}/models/{}/imports/{}/tasks'.format(workspace_id, model_id, action_id)
        return (requests.post(import_url, headers=import_headers,data=json.dumps({"localeName": "en_US"})))
    elif action_type == 'exports':
        print('exports')
        export_headers = {}
        export_headers['Authorization'] = 'AnaplanAuthToken ' + auth_token
        export_headers['Content-Type'] = 'application/json'
        export_url = 'https://api.anaplan.com/2/0/workspaces/{}/models/{}/exports/{}/tasks'.format(workspace_id, model_id, action_id)
        print(requests.post(export_url, headers=export_headers,data=json.dumps({"localeName": "en_US"})))
        time.sleep(5)
        chunk_url = 'https://api.anaplan.com/2/0/workspaces/{}/models/{}/files/{}/chunks/0'.format(workspace_id, model_id, action_id)
        return requests.get(chunk_url, headers=export_headers)
        



def test_func():
    print('this is a test of the helper module')
    return


def test_func2():
    return 'test of helper'