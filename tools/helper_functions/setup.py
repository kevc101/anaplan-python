from setuptools import setup

setup(name='helper_functions',
      version='0.1',
      description='Helper Functions for Anaplan API for GPT',
      author='Kevin Cho',
      author_email='kevin.cho@gpt.com.au',
      packages=['helper_functions'],
      zip_safe=False)